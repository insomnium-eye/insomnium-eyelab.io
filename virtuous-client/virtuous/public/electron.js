const {app, BrowserWindow} = require('electron');

const path = require('path');
const url = require('url');

let mainWindow;

function createWindow() {
  mainWindow = new BrowserWindow({width: 900, height: 550,
	  icon: path.join(__dirname, '/../build/android-chrome-512x512.png')});

  mainWindow.loadURL(url.format({
		  pathname: path.join(__dirname, '/../build/index.html'),
		  protocol: 'file:',
		  slashes: true
	  }));
  mainWindow.on('closed', () => mainWindow = null);
}

app.on('ready', createWindow);

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow();
  }
});

app.on('browser-window-created', (e,window) => {
  window.setMenu(null);
});
