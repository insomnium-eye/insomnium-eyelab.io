import React, { Component } from "react";
import "./Home.css";

export default class Home extends Component {
	render() {
		return (
			<div className="Home">
				<div className="lander">
					<h1>{ window.app_title }</h1>
					<p>A client database.</p>
				</div>
			</div>
		);
	}
}
