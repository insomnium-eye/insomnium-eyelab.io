<?php

// sudo apt-get install php-imagick
// sudo systemctl restart apache2

require_once 'inc/utilities.php';

cors();

$errors = array();
$data = array();

if (!isInstalled()) {
	$errors['not_installed'] = $app_title . ' server is not installed.';
}

$url = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
	$json = json_decode(file_get_contents('php://input'), true);
	$url = trim($json['url']);
	if (empty($url)) {
		$errors['url'] = 'No url provided.';
	}
} else {
	$errors['post'] = 'Must send data over POST request method.';
}

if (empty($errors)) {
	// Algorithm from https://gist.github.com/paulferrett/8098215
	try {
		// Read image file with Image Magick
		$image = new Imagick($url);
		// Scale down to 1x1 pixel to make Imagick do the average
		$image->scaleimage(1, 1);
		// @var ImagickPixel $pixel
		if(!$pixels = $image->getimagehistogram()) {
			$errors['histogram'] = "Failed to get the image histogram.";
		}
		$pixel = reset($pixels);
		$rgb = $pixel->getcolor();

		$data['rgb'] = $rgb;
	} catch(ImagickException $e) {
		$errors['ImagickException'] = $e->getMessage();
	} catch(Exception $e) {
		$errors['Exception'] = $e->getMessage();
	}
}

if ( ! empty($errors)) {
	$data['errors']  = $errors;
	$data['success'] = false;
} else {
	$data['success'] = true;
}

echo json_encode($data);

?>
