## Requirements

* Node >= 6

Ubuntu install:
```sh
$ sudo apt install nodejs npm
$ sudo npm install -g create-react-app
```

## Setting up the project

Tutorial: https://reactjs.org/docs/add-react-to-a-new-app.html

```sh
$ cd virtuous-client
$ create-react-app virtuous # This creates the react app and installs all the necessary packages (react, react-dom, react-scripts)
```
Output:
```
Success! Created virtuous at /var/www/html/virtuous/virtuous-client/virtuous
Inside that directory, you can run several commands:

  npm start
    Starts the development server.

  npm run build
    Bundles the app into static files for production.

  npm test
    Starts the test runner.

  npm run eject
    Removes this tool and copies build dependencies, configuration files
    and scripts into the app directory. If you do this, you can’t go back!

We suggest that you begin by typing:

  cd virtuous
  npm start
```

### Install dependencies

```sh
$ npm install react-bootstrap --save
$ npm install react-router-dom --save
$ npm install react-router-bootstrap --save
$ npm install --save-dev electron electron-builder foreman

# Font Awesome - https://github.com/FortAwesome/react-fontawesome
$ npm i --save @fortawesome/fontawesome-svg-core@prerelease
$ npm i --save @fortawesome/free-solid-svg-icons@prerelease
$ npm i --save @fortawesome/react-fontawesome@prerelease

# Confirm alert
$ npm install react-confirm-alert --save
$ npm install react-hot-loader --save

# Read Spreadsheet
$ npm install xlsx --save
$ npm install react-file-reader --save
```

## Cloning the project

When cloning the project from git, we'll need to install the dependencies by running `npm install` on the project root folder (where the file `package.json` is located).

## Run App

__**Make sure the server string is set to localhost in src/containers/Login.js and not the insomnium-eye server!**__

In browser:
```sh
$ npm start
```

In browser and electron:

On `package.json` add `"main": "src/electron-starter.js",` and `"homepage": "./",`.  
*(Note: This has to be removed when building the app for production.)*
```sh
$ npm run dev
```

## Build App

### Electron

1. __**Make sure the server string is set to the insomnium-eye server in src/containers/Login.js and not localhost**__
2. On `package.json` add `"main": "public/electron.js,"` and `"homepage": "./",`.

```sh
$ npm run build
```

Build electron for Linux:
```sh
$ npm run electron-pack
```

Build electron for Windows:
```sh
$ npm run electron-pack-win
```

Build electron for Mac:  
*(Currently gives error. But since this isn't important I have ignored this platform.)*
```sh
$ npm run electron-pack-mac
```

### Linux host (eg. RPI3):

1. __**Make sure the server string is set to the insomnium-eye server in src/containers/Login.js and not localhost**__
2. Remove `main`, `homepage`, `devDependencies` and `build` from `package.json`
3. https://hackernoon.com/start-to-finish-deploying-a-react-app-on-digitalocean-bcfae9e6d01b
	```sh
	$ sudo ufw allow 8080/tcp # Also port forward on router
	$ sudo npm i -g pm2 # One-time install
	$ sudo npm i -g serve # One-time install
	$ npm install #One-time install
	$ npm run build
	$ pm2 serve build # http://pm2.keymetrics.io/docs/usage/quick-start/
	```

Website should be available on http://yourwebsite:8080

For further instructions (eg. custom dns without need to specify port and https) check: https://hackernoon.com/start-to-finish-deploying-a-react-app-on-digitalocean-bcfae9e6d01b


### GitLab:

1. __**Make sure the server string is set to the insomnium-eye server in src/containers/Login.js and not localhost**__
2. On `package.json` remove `main` and add `"homepage": ".",`.
3. Make sure there is a `.gitlab-ci.yml` file in the root of the project
4. Make sure the repository name and path are both set to `username.gitlab.io`
5. Just push the project to the repository (`git push`).

Tutorials:
- https://gist.github.com/josephrace/719bfc7e40f632fd7aa3940ef1ede59e
- https://about.gitlab.com/features/pages/
- https://docs.gitlab.com/ee/user/project/pages/introduction.html#user-or-group-pages

## Credits

- Bootstrap theme taken from https://bootswatch.com/3/.  
*(Had to find and replace `../fonts` with `./../fonts` on `src/css/bootstrap.min.css` file)*
- Wallpaper taken from https://wallup.net/landscape-parking-lot-architecture-urban-sunset-empty-modern/

## Inspiration

- Electron:
	- https://medium.freecodecamp.org/building-an-electron-application-with-create-react-app-97945861647c
	- https://medium.com/@kitze/%EF%B8%8F-from-react-to-an-electron-app-ready-for-production-a0468ecb1da3
	- https://www.electron.build/configuration/configuration
	- https://www.electron.build/multi-platform-build
- Preloader: https://stackoverflow.com/questions/40987309/react-display-loading-screen-while-dom-is-rendering
- Tutorials:
	- https://serverless-stack.com
	- https://auth0.com/blog/reactjs-authentication-tutorial/
	- CORS: https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS
	- package.json: https://docs.npmjs.com/files/package.json#people-fields-author-contributors
