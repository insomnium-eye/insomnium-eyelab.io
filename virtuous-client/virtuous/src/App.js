import React, { Component } from 'react';
import { Link } from "react-router-dom";
import { LinkContainer } from "react-router-bootstrap";
import { Nav, Navbar, NavItem, NavDropdown, MenuItem,
 	Modal, OverlayTrigger, Tooltip, Radio,
 	FormGroup, FormControl, ControlLabel, Button } from "react-bootstrap";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faExclamationTriangle, faInfoCircle } from '@fortawesome/free-solid-svg-icons';
import './App.css';
import Routes from "./Routes";
import Preloader from './Preloader';
import { loaderStart, loaderEnd, postData, setBgImg, setVarColors,
	setCookie, getCookie, PlayYTMusic } from "./utils.js";
import ReactDOM from 'react-dom';
import Login from "./containers/Login";

class App extends Component {

	constructor(props) {
		super(props);

		let bgImg = "";
		let bgImgStyle = "zoomed";
		let ytMus = "";
		let bgColorType = true;// auto
		let bgColor = "#2b3e50";

		/* Set bg color before bg image */
		if (getCookie("bgColorType")) {
			bgColorType = (getCookie("bgColorType") === 'true');
		}
		if (getCookie("bgColor")) {
			bgColor = getCookie("bgColor");
		}
		if (getCookie("bgImg")) {
			bgImg = getCookie("bgImg");
			if (getCookie("bgImgStyle")) {
				bgImgStyle = getCookie("bgImgStyle");
			}
		}
		if (getCookie("ytMus")) {
			ytMus = getCookie("ytMus");
		}

		this.state = {
			bgImg: bgImg,
			bgImgStyle: bgImgStyle,
			bgColorType: bgColorType,
			bgColor: bgColor,
			ytMus: ytMus,
			settingsShow: false
		};
	}

	componentDidMount() {
		PlayYTMusic("");
		setTimeout(() => {
			try {
				const ele = document.getElementById('preloader')
				if(ele) {
					// fade out
					ele.classList.add('available')
					setTimeout(() => {
						// remove from DOM
						try {
							ele.outerHTML = ''
						} catch (e) {}
					}, 700)
				}
			} catch (e) {}
		}, 2000);
	}

	logout = event => {
		loaderStart();
		postData("logout.php", {})
		.then((data) => {
			if (!data.success) {
				Object.keys(data.errors).forEach((k) => {
					console.log("Error: " + data.errors[k]);
					if (data.errors[k].includes("You are not logged in!")) {
						ReactDOM.render(
							<Login />,
							document.getElementById("root")
						);
					}
				});
			} else {
				//console.log("Success: " + data.message);
				ReactDOM.render(
					<Login />,
					document.getElementById("root")
				);
			}
			loaderEnd();
		})
		.catch((error) => {
			console.log("Error: " + error);
			if (error.includes("You are not logged in!")) {
				ReactDOM.render(
					<Login />,
					document.getElementById("root")
				);
			}
			loaderEnd();
		});

		event.preventDefault();
	}

	/**
	 * Navbar: https://react-bootstrap.github.io/components/navbar/
	 * eventKey: https://github.com/react-bootstrap/react-bootstrap/issues/320#issuecomment-260098771
	 * (sets "active" tab)
	 * LinkContainer: https://stackoverflow.com/questions/35687353/react-bootstrap-link-item-in-a-navitem
	 */
	render() {
		return (
			<div className="App container">
				<Preloader />
				<Navbar fluid collapseOnSelect>
					<Navbar.Header>
						<Navbar.Brand>
							<Link to="/">{ window.app_title }</Link>
						</Navbar.Brand>
						<Navbar.Toggle />
					</Navbar.Header>
					<Navbar.Collapse>
						<Nav>
							<LinkContainer to="/clients">
								<NavItem eventKey={1}>Clients</NavItem>
							</LinkContainer>
						</Nav>
						<Nav pullRight>
							<NavDropdown eventKey={2} title={""} id="nav-user-menu"
								data-username={this.props.username}>
								<MenuItem eventKey={2.1}
									onClick={this.handleShow}>
									Settings
								</MenuItem>
								<MenuItem eventKey={2.2}
									onClick={this.logout}>
									Logout
								</MenuItem>
							</NavDropdown>
						</Nav>
						<Navbar.Text pullRight id="welcomeUser">
							Welcome, <Navbar.Link href="#"><strong>{this.props.username}</strong></Navbar.Link>!
						</Navbar.Text>
					</Navbar.Collapse>
				</Navbar>
				<Routes />
				<div id="loader" className="loader"></div>
				<Modal show={this.state.settingsShow} onHide={this.handleClose}>
					<Modal.Header closeButton>
						<Modal.Title>Settings</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<p>
							<strong>Local settings:</strong>
							<OverlayTrigger placement="top" overlay={(
								<Tooltip id="localInfo">
									These settings are <strong>not</strong> saved across multiple devices.
									For resetting them to default clear your browser cookies.
								</Tooltip>
							)}>
								<FontAwesomeIcon icon={faInfoCircle}
									style={{color: "#acf", marginLeft: "10px", marginBottom: "-1px"}} />
							</OverlayTrigger>
						</p>
						<FormGroup controlId="bgImg">
							<ControlLabel>Background image URL:</ControlLabel>
							<FormControl
								onChange={e => this.setState({ bgImg: e.target.value })}
								type="text"
								value={this.state.bgImg}
								placeholder="Background image..."
							/>
						</FormGroup>
						<FormGroup controlId="bgImgStyle">
							<ControlLabel>Background style:</ControlLabel>
							<FormControl componentClass="select" placeholder="style"
								value={this.state.bgImgStyle}
								onChange={e => this.setState({ bgImgStyle: e.target.value })}>
								<option value="none">None</option>
								<option value="centered">Centered</option>
								<option value="tiled">Tiled</option>
								<option value="stretched">Stretched</option>
								<option value="scaled">Scaled</option>
								<option value="zoomed">Zoomed</option>
							</FormControl>
						</FormGroup>
						<FormGroup controlId="bgColor">
							<ControlLabel>Background color:</ControlLabel><br />
							<Radio name="radioColor"
								defaultChecked={this.state.bgColorType}
								onChange={e => this.setState({ bgColorType: e.target.checked })}
								inline>
								Auto
							</Radio>{' '}
							<Radio name="radioColor"
								defaultChecked={!this.state.bgColorType}
								onChange={e => this.setState({ bgColorType: !e.target.checked })}
								inline>
								Color
							</Radio>{' '}
							<input type="color" id="head" name="color"
								value={this.state.bgColor}
								onChange={e => this.setState({ bgColor: e.target.value })} />
						</FormGroup>
						<FormGroup controlId="ytMus">
							<ControlLabel>Login music (YouTube URL):
								<OverlayTrigger placement="top" overlay={(
									<Tooltip id="YTTooltip">
										Autoplay is disabled in Chrome by default &mdash; music
										will not play upon opening the app. If you wish
										to use this feature you must go to <a href="chrome://flags/#autoplay-policy">
										chrome://flags/#autoplay-policy</a> and change the "Autoplay policy" to "No user gesture is required".
									</Tooltip>
								)}>
									<FontAwesomeIcon icon={faExclamationTriangle}
										style={{color: "yellow", marginLeft: "10px"}} />
								</OverlayTrigger>
							</ControlLabel>
							<FormControl
								onChange={e => this.setState({ ytMus: e.target.value })}
								type="text"
								value={this.state.ytMus}
								placeholder="YouTube URL..."
							/>
						</FormGroup>
					</Modal.Body>
					<Modal.Footer>
						<Button onClick={this.handleSave}>Save</Button>
					</Modal.Footer>
				</Modal>
			</div>
		);
	}

	handleClose = event => {
		if (getCookie("bgImg")) {
			this.setState({ bgImg: getCookie("bgImg") });
		} else {
			this.setState({ bgImg: "" });
		}
		if (getCookie("bgImgStyle")) {
			this.setState({ bgImgStyle: getCookie("bgImgStyle") });
		} else {
			this.setState({ bgImgStyle: "zoomed" });
		}
		if (getCookie("bgColorType")) {
			this.setState({ bgColorType: (getCookie("bgColorType") === 'true') });
		} else {
			this.setState({ bgColorType: true });
		}
		if (getCookie("bgColor")) {
			this.setState({ bgColor: getCookie("bgColor") });
		} else {
			this.setState({ bgColor: "#2b3e50" });
		}
		if (getCookie("ytMus")) {
			this.setState({ ytMus: getCookie("ytMus") });
		} else {
			this.setState({ ytMus: "" });
		}
		this.setState({ settingsShow: false });
	}

	handleSave = event => {
		this.setState({ error: '' });
		/* Set bg color before bg image */
		setCookie("bgColorType", this.state.bgColorType, 365*5);
		window.bgColorType = this.state.bgColorType;
		setCookie("bgColor", this.state.bgColor, 365*5);
		if(!this.state.bgColorType) {
			document.documentElement.style.setProperty('--bodyBg', this.state.bgColor);
		}
		setCookie("bgImg", this.state.bgImg, 365*5);
		setCookie("bgImgStyle", this.state.bgImgStyle, 365*5);
		setBgImg(this.state.bgImg, this.state.bgImgStyle );
		setVarColors(this, this.state.bgImg, this.state.bgImgStyle);
		setCookie("ytMus", this.state.ytMus, 365*5);
		this.setState({ settingsShow: false });
	}

	handleShow = event => {
		this.setState({ settingsShow: true });
	}

}

export default App;
