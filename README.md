## Functionality

The app holds a database with clients.

## Screenshots

![clients](screenshots/login.jpg)
![clients](screenshots/main.jpg)
![clients](screenshots/clients.jpg)


## Naming Conventions

https://www.w3schools.com/js/js_conventions.asp

The only exception to the rules above is that we use tabs for indentation and spaces
for alignment. Eg.

![](http://i.imgur.com/p2u0XMs.png)

**JavaScript good practices**

1. Place `"use strict";` on top of every JavaScript file.
2. Use `let` instead of `var` for variables.
3. Use strict equality operator `===` and `!==` for comparisons instead of regular equality operator `==` and `!=`.
4. Use Automated Linters (JSLint, JSHint, ESLint). ~~I also use `linter-gjslint` on Atom editor.~~ I use `linter-eslint` because it is compatible with ReactJS.
5. Test using [Mocha](https://mochajs.org/), [Chai](http://www.chaijs.com/), [Sinon](http://sinonjs.org/). Tutorial [here](https://javascript.info/testing-mocha).
6. Use [Babel](https://babeljs.io/) for compatibility with older browsers.

**Interesting ES6 JavaScript tricks**

*Tutorial: http://ccoenraets.github.io/es6-tutorial/*

1. [Template literals](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Template_literals)
2. Exponentiation operator `2 ** 3 = 2 * 2 * 2`.
3. Unary `+` operator coverts string to number.
4. Double NOT `!!` converts value to boolean.
5. [Labels](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/label)
6. [Arrow functions](https://www.sitepoint.com/es6-arrow-functions-new-fat-concise-syntax-javascript/)
7. [Debugging](https://javascript.info/debugging-chrome)

**ReactJS tips**

1. Install [ReactJS add-on](https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi) on browser for helping with development.

---

_Copyright &copy; 2018 Insomnium Eye_