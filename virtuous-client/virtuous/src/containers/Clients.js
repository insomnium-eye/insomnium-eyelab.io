import React, { Component } from "react";
// https://react-bootstrap.github.io/layout/grid/
import { Grid, Row, Col, Button, FormGroup,
	ControlLabel, FormControl, OverlayTrigger, Popover, Tooltip  } from "react-bootstrap";
import XLSX from 'xlsx';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBuilding, faMapMarkerAlt,
	faPhone, faIdCardAlt, faQuestion,
	faCheck, faTimes } from '@fortawesome/free-solid-svg-icons';
import ClientCard from "./ClientCard"
import ReactFileReader from 'react-file-reader';
import { loaderStart, loaderEnd, postData, b64toBlob, nextChar } from "../utils.js";
import "./Clients.css";
import excelImage from '../images/excel.png';

export default class Clients extends Component {

	constructor(props) {
		super(props);

		this.state = {
			clients: "",
			error: "",
			newClient: false,
			spreadsheetCount: 0,
			spreadsheetNum: Number.MAX_SAFE_INTEGER
		};
	}

	componentDidMount() {
		this.listClients();
	}

	render() {

		let error = <br key="br2" />;

		if (this.state.error) {
			Object.keys(this.state.error).forEach((k) => {
				error = [error, <div className="alert alert-danger fade in" key={k} data-key={k}>
					<a className="close"
						data-dismiss="alert"
						aria-label="close"
						onClick={() => this.handleDismiss(k)}>&times;</a>
					{ this.state.error[k] }
				</div>]
			});
		}

		var rows = [];
		if (this.state.clients) {
			Object.keys(this.state.clients).forEach((k) => {
				// note: we add a key prop here to allow react to uniquely identify each
				// element in this array. see: https://reactjs.org/docs/lists-and-keys.html
				rows.push(
					<ClientCard key={k + Date.now()}
						cid={this.state.clients[k].cid}
						name={this.state.clients[k].name}
						profession={this.state.clients[k].profession}
						company={this.state.clients[k].company}
						address={this.state.clients[k].address}
						license_no={this.state.clients[k].license_no}
						email={this.state.clients[k].email}
						phone={this.state.clients[k].phone}
						notes={this.state.clients[k].notes}
					/>
				);
			});
		}

		// Must set the height on the popover so that it calculates the placement correctly
		const popoverRight = (
			<Popover style={{height: "280px"}} id="popover-positioned-right" title="Spreadsheet Format">
				<ol>
					<li>Spreadsheet must be in .xlsx format.</li>
					<li>Parser reads the first worksheet in the workbook.</li>
					<li>Headers must begin in the first cell of the worksheet (A1).</li>
					<li>
						Valid header names are <code>name</code>, <code>profession</code>,
						<code>company</code>, <code>address</code>, <code>license_no</code>,
						<code>phone</code>, <code>email</code>, <code>notes</code>.
						They are case-sensitive.
					</li>
					<li>
						Not all headers need to be specified and their order is
						irrelevant, except for the name header which is mandatory and
						must be in the first column (A).
					</li>
					<li>
						The parser will stop at the first blank cell when reading the
						headers and the clients. Thus, if there is an empty column
						between headers (i.e. a column without header), all the headers / columns
						on its right side will be ignored. The same applies for the client rows &ndash;
						if there is an empty row between clients, the clients below will be ignored.
					</li>
				</ol>
				<img src={excelImage} alt="spreadsheet format" />
			</Popover>
		);

		return (
			<div className="Clients">
				<h2>Clients</h2>
				<hr />
				{ error }
				<div id="options" className="clearfix">
					<Button
						id="insertClient"
						type="button"
						onClick={this.insertClient}
					>
						<span className="buttontext">Insert Client</span>
					</Button>
					<div id="importSpreadsheet">
						<ReactFileReader
							base64={true}
							fileTypes={[".xlsx"]}
							handleFiles={this.handleFiles}>
							<Button type="button">
								<span className="buttontext">Import Spreadsheet</span>
							</Button>
						</ReactFileReader>
						<OverlayTrigger trigger="click"
							placement={window.innerWidth > 600 ? "right" : "bottom"}
							overlay={popoverRight}>
							<Button id="helpSpreadsheet" title="Help" type="button">
								<FontAwesomeIcon icon={faQuestion} />
							</Button>
						</OverlayTrigger>
					</div>
					<div id="optionForms">
						<form id="orderForm" onSubmit={this.handleSort} method="post">
							<FormGroup id="sortSelect">
								<ControlLabel>Sort by:</ControlLabel>
								<FormControl componentClass="select" placeholder="select">
									<option value="name">Name</option>
									<option value="created_at">Creation date</option>
								</FormControl>
							</FormGroup>
							<label className="radio-inline">
								<input type="radio" name="order" value="asc" defaultChecked />ascending order
							</label>
							<label className="radio-inline">
								<input type="radio" name="order" value="desc" />descending order
							</label>
							<Button id="sortClients" type="submit">Sort</Button>
						</form>
						<form id="searchForm" onSubmit={this.handleSearch} method="post">
							<FormGroup id="searchSelect">
								<ControlLabel>Search by:</ControlLabel>
								<br />
								<FormControl id="searchSelect" componentClass="select" placeholder="select">
									<option value="name">Name</option>
									<option value="profession">Profession</option>
									<option value="company">Company</option>
									<option value="address">Address</option>
									<option value="license_no">License</option>
									<option value="phone">Phone</option>
									<option value="email">Email</option>
									<option value="notes">Notes</option>
								</FormControl>
								<Button id="searchClients" type="submit">Search</Button>
								<FormControl
									id="searchText"
									type="text"
									placeholder="Search"
								/>
							</FormGroup>
						</form>
					</div>
				</div>
				<Grid id="grid">
					<Row className="show-grid">
						{ this.state.newClient ? this.newClientForm() : null }
						{rows}
					</Row>
				</Grid>
			</div>
		);
	}


	handleDismiss(i) {
		let error = this.state.error;
		delete error[i];
		this.setState({error: error});
	}

	appendToError(obj) {
		let str = JSON.stringify(obj);
		str = str.replace(/"(.)*":/g, "\"" + Date.now() + "2$1\":");
		let json = JSON.parse(str);

		let error = Object.assign(json, this.state.error);

		this.setState({ error: error });
	}

	handleSort = event => {
		this.listClients();
		event.preventDefault();
	}

	handleSearch = event => {
		this.listClients();
		event.preventDefault();
	}

	insertClient = event => {
		if (this.state.newClient === false) {
			this.setState({newClient: true});
		}
	}
	cancelInsertClient = event => {
		if (this.state.newClient === true) {
			this.setState({error: ""});
			this.setState({newClient: false});
		}
	}

	saveNewClient = event => {

		loaderStart();
		this.setState({error: ""});
		postData("clients.php", {
			action: "insert",
			name: this.newName.innerText,
			profession: this.newProfession.innerText,
			company: this.newCompany.innerText,
			address: this.newAddress.innerText,
			license_no: this.newLicense.innerText,
			phone: this.newPhone.innerText,
			email: this.newEmail.innerText,
			notes: this.newNote.innerText
		})
		.then((data) => {
			if (!data.success) {
				this.appendToError(data.errors);
			} else {
				this.setState({newClient: false});
				// It is important that we list clients after insert so that we get
				// the client id to update/delete this client afterwards
				this.listClients();
			}
			loaderEnd();
		})
		.catch((error2) => {
			this.appendToError({ invalid: 'Error: ' + error2 });
			console.log(error2);
			loaderEnd();
		});
	}

	listClients() {
		loaderStart();

		let sort = this.getSortValue();
		let order = this.getOrderValue();
		let searchOption = this.getSearchOption();
		let searchText = this.getSearchText();

		postData("clients.php", {
			action: "list",
			sort: sort,
			order: order,
			searchOption: searchOption,
			searchText: searchText
		})
		.then((data) => {
			if (!data.success) {
				this.appendToError(data.errors);
			} else {
				this.setState({ clients: data.message });
			}
			loaderEnd();
		})
		.catch((error2) => {
			this.appendToError({ invalid: 'Error: ' + error2 });
			console.log(error2);
			loaderEnd();
		});
	}

	getSortValue() {
		try {
			let e = document.getElementById("sortSelect");
			return e.options[e.selectedIndex].value;
		} catch (e) { return "name"; }
	}

	getOrderValue() {
		try {
			let radios = document.getElementsByName('order');

			for (let i = 0, length = radios.length; i < length; i++) {
				if (radios[i].checked) {
					return radios[i].value;
				}
			}
		} catch (e) { return "asc"; }
	}

	getSearchOption() {
		try {
			let e = document.getElementById("searchSelect");
			return e.options[e.selectedIndex].value;
		} catch (e) { return ""; }
	}

	getSearchText() {
		try {
			return document.getElementById("searchText").value;
		} catch (e) { return ""; }
	}

	newClientForm() {

		return (
			<Col sm={6} md={6} lg={4} id="newClientForm">
				<div className="card">
					{/*img className="avatar-cover" src={avatar} alt="Jane" /> */ }
					<div className="container">
						<h2 contentEditable="true"
							data-placeholder="Insert name..."
							ref={r => this.newName = r}>
							{ /* This comment is for eslint to not complain about empty header */ }
						</h2>
						<p className="title"
							contentEditable="true"
							data-placeholder="Insert profession..."
							ref={r => this.newProfession = r}></p>
						<table className="clientDetails">
							<tbody>
								<tr>
									<td><FontAwesomeIcon icon={faBuilding} /></td>
									<td contentEditable="true"
										ref={r => this.newCompany = r}
										data-placeholder="Insert company..."></td>
									<td><FontAwesomeIcon icon={faMapMarkerAlt} /></td>
									<td contentEditable="true"
										ref={r => this.newAddress = r}
										data-placeholder="Insert address..."></td>
								</tr>
								<tr>
									<td><FontAwesomeIcon icon={faIdCardAlt} /></td>
									<td contentEditable="true"
										ref={r => this.newLicense = r}
										data-placeholder="Insert license..."></td>
									<td><FontAwesomeIcon icon={faPhone} /></td>
									<td contentEditable="true"
										ref={r => this.newPhone = r}
										data-placeholder="Insert phone..."></td>
								</tr>
							</tbody>
						</table>
						<p contentEditable="true"
							ref={r => this.newEmail = r}
							data-placeholder="Insert email..."></p>
						<p>
							<strong>Notes: </strong>
							<span contentEditable="true"
								ref={r => this.newNote = r}
								data-placeholder="Insert note..."></span>
						</p>
						<p>
							<OverlayTrigger placement="top" overlay={(
								<Tooltip id="saveTooltip">
									Save
								</Tooltip>
							)}>
								<Button className="saveBtn" style={{marginRight:"15px"}}
									type="button"
									onClick={this.saveNewClient}>
									<FontAwesomeIcon icon={faCheck} />
								</Button>
							</OverlayTrigger>
							<OverlayTrigger placement="top" overlay={(
								<Tooltip id="cancelTooltip">
									Cancel
								</Tooltip>
							)}>
								<Button type="button" className="cancelBtn"
									onClick={this.cancelInsertClient}>
									<FontAwesomeIcon icon={faTimes} />
								</Button>
							</OverlayTrigger>
						</p>
					</div>
				</div>
			</Col>
		);
	}

	insertClientSpreadsheet(props) {

		postData("clients.php", {
			action: "insert",
			name: props.name,
			profession: props.profession,
			company: props.company,
			address: props.address,
			license_no: props.license_no,
			phone: props.phone,
			email: props.email,
			notes: props.notes
		})
		.then((data) => {
			if (!data.success) {
				this.appendToError(data.errors);
			}
			this.setState({spreadsheetCount: this.state.spreadsheetCount + 1});
			if (this.state.spreadsheetCount >= this.state.spreadsheetNum) {
				this.setState({spreadsheetCount: 0, spreadsheetNum: Number.MAX_SAFE_INTEGER});
				loaderEnd();
				// It is important that we list clients after insert so that we get
				// the client id to update/delete this client afterwards
				this.listClients();
			}
		})
		.catch((error2) => {
			this.appendToError({ invalid: 'Error: ' + error2 });
			console.log(error2);
			this.setState({spreadsheetCount: this.state.spreadsheetCount + 1});
			if (this.state.spreadsheetCount >= this.state.spreadsheetNum) {
				this.setState({spreadsheetCount: 0, spreadsheetNum: Number.MAX_SAFE_INTEGER});
				loaderEnd();
				// It is important that we list clients after insert so that we get
				// the client id to update/delete this client afterwards
				this.listClients();
			}
		});
	}


	// https://stackoverflow.com/questions/46909260/reading-excel-file-in-reactjs
	// https://github.com/SheetJS/js-xlsx
	handleFiles = files => {
		//console.log(files.fileList + files.base64)
		var contentType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';

		var blob = b64toBlob(files.base64, contentType);

		const reader = new FileReader();
		reader.onload = (evt) => { //evt = on_file_select event

			loaderStart();
			this.setState({error: ""});

			/* Parse file data */
			const bstr = evt.target.result;
			const wb = XLSX.read(bstr, {type:'binary'});

			/* Get first worksheet */
			const wsname = wb.SheetNames[0]; // First worksheet name
			const ws = wb.Sheets[wsname]; // Get sheet by name

			const validColumnNames = ['name','profession','company','address',
				'license_no','phone','email','notes']
			let columns = [];
			const firstHeading = (ws['A1'] ? ws['A1'].v : undefined);
			if (firstHeading === undefined) {
				this.appendToError({spreadsheet: "First column has empty header. Aborting."});
				loaderEnd();
				return;
			}
			for(let i = 'A'; true; i = nextChar(i)) {
				const heading = (ws[i + '1'] ? ws[i + '1'].v : undefined);
				if (heading === undefined) {
					break;
				}
				else if(! validColumnNames.includes(heading)) {
					this.appendToError({spreadsheet: `Invalid column '${heading}'. Aborting.`});
					loaderEnd();
					return;
				}
				else {
					columns.push(heading);
				}
			}
			if(! columns.includes("name")) {
				this.appendToError({spreadsheet: "Spreadsheet must contain a 'name' column. Aborting."});
				loaderEnd();
				return;
			}

			let k;

			loop1:
			for(k = 2; true; k++) {
				let props = {};
				for (let i = 'A', j=0; j < columns.length; i = nextChar(i), j++) {
					const cell = (ws[i + k] ? ws[i + k].v : '');
					if (cell === '' && j === 0) break loop1;
					props[columns[j]] = cell;
				}

				this.insertClientSpreadsheet(props);
			}

			this.setState({spreadsheetNum: k-2});

			/* Convert array of arrays */
			//const data = XLSX.utils.sheet_to_json(ws, {header:1});
			/* Update state */
			//console.log("Data>>>"+data);
		};
		reader.readAsBinaryString(blob);
	}

}
