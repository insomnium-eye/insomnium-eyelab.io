import React, { Component } from 'react';
import logo from './images/logo.png';
import './Preloader.css';

class Preloader extends Component {

	render() {
		return (
			<div id="preloader" className="page-preloader preloader-wrapp">
				<img src={logo} alt="logo" />
				<div className="preloader"></div>
			</div>
		);
	}
}

export default Preloader;
