<?php

/* Copyright (c) 2018 Insomnium Eye */

require_once '../inc/config.php';

$errors = array();
$data = array();


if ( empty(trim($_REQUEST["user"])) ) {
	$errors['user'] = 'User is required.';
}
if ( empty(trim($_REQUEST["pwd"])) ) {
	$errors['pwd'] = 'Password is required.';
}
if ( empty(trim($_REQUEST["pwdconf"])) ) {
	$errors['pwdconf'] = 'Please confirm password.';
}

$user = trim($_REQUEST["user"]);
$pwd = trim($_REQUEST["pwd"]);
$pwdconf = trim($_REQUEST["pwdconf"]);

if(strlen($pwd) < 8 ) {
	$errors['pwd'] = 'Password must have a minimum of 8 characters.';
}
if($pwd != $pwdconf){
	$errors['pwdconf'] = 'Password did not match.';
}

if(!defined('DB_SERVER') || !defined('DB_NAME') || !defined('DB_USERNAME')
   || !defined('DB_PASSWORD')) {
	$errors['not_installed'] = $app_title . ' is not installed.';
}

if ( ! empty($errors)) {
	$data['errors']  = $errors;
	$data['success'] = false;
} else {

	try {
		$conn = new PDO("mysql:host=" .
		                 constant("DB_SERVER") . ";dbname=" .
						 constant("DB_NAME"), constant("DB_USERNAME"),
						 constant("DB_PASSWORD"));
		// set the PDO error mode to exception
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		$stmt = $conn->prepare("INSERT INTO users (username, password, admin)" .
		                       "VALUES (:username, :password, 1)");

		$pwdhashed = password_hash($pwd, PASSWORD_DEFAULT);
		$stmt->bindParam(':username', $user);
		$stmt->bindParam(':password', $pwdhashed);

		$stmt->execute();

		$data['message'] = "User created successfully.";
		$data['success'] = true;
	}
	catch(PDOException $e) {
		$errors['exception'] = $e->getMessage();
		$data['errors'] = $errors;
		$data['success'] = false;
	}
	$conn = null;
}

echo json_encode($data);

?>
