<?php

/* Copyright (c) 2018 Insomnium Eye */

define('PROJECT_ROOT', dirname(dirname(__FILE__))."/");

require_once PROJECT_ROOT.'inc/config.php';

?>

<!DOCTYPE html>

<html lang="en">
<head>
	<title>Uninstall <?php echo $app_title; ?></title>
	<meta charset="UTF-8">
	<meta name="keywords" content="Insomnium-Eye, Isomnium Eye, IE">
	<meta name="author" content="Insomnium-Eye">
	<link rel="shortcut icon" href="../favicon.png" type="image/png">

	<!--

	<meta name="description" content="Description">
	<link rel="shortcut icon" href="favicon.png" type="image/png">

	<link rel="stylesheet" href="style.css">

	<script type="text/javascript">
	</script>

	-->
	<style type="text/css">
		a.disabled {
			pointer-events: none;
		}
	</style>

	<!-- Bootstrap, Font Awesome -->
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"
	      integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB"
	      crossorigin="anonymous">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css"
	integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">

</head>

<body>
	<div class="col-sm-6 offset-sm-3">
		<h1>Uninstall <?php echo $app_title; ?></h1>
		<ul class="nav nav-tabs" role="tablist">
			<li class="nav-item">
				<a class="nav-link active" id="database-tab" data-toggle="tab"
				   href="#database" role="tab" aria-controls="database"
				   aria-selected="true">1. Delete database</a>
			</li>
			<li class="nav-item">
				<a class="nav-link disabled" id="config-tab" data-toggle="tab"
				   href="#config" role="tab" aria-controls="config"
				   aria-selected="true">2. Remove configuration file</a>
			</li>
			<li class="nav-item">
				<a class="nav-link disabled" id="done-tab" data-toggle="tab"
				   href="#done" role="tab" aria-controls="done"
				   aria-selected="true">3. Done</a>
			</li>
		</ul>
		<div class="tab-content">
			<div id="database" class="tab-pane fade show active"
			     role="tabpanel" aria-labelledby="database-tab">
				<div class="alert alert-warning" role="alert">
					Attention! All data will be lost after this operation.
				</div>
				<button id="remove-db" type="button" class="btn btn-default">
					 Remove database.</button>
			</div>
			<div id="config" class="tab-pane fade"
			     role="tabpanel" aria-labelledby="config-tab">
				<br />
				<pre id="remove-file"></pre>
				<button id="remove-config" type="button" class="btn btn-default">
					 Remove configuration file.</button>
			</div>
			<div id="done" class="tab-pane fade"
			     role="tabpanel" aria-labelledby="user-tab">
				 <br /><div class="alert alert-success">Application uninstalled successfully.</div>
			</div>
		</div>
		<!--<span id="next" class="d-inline-block" tabindex="0"
		      data-toggle="tooltip" data-placement="top"
		      title="You must create the tables before proceeding."
		      style="float:right;">
		<button type="button" class="btn btn-default"
		        style="pointer-events: none;"
		        disabled>
			Next <span class="fas fa-arrow-right"></span></button>
		</span>-->
	</div>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
	        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
	        crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"
	        integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T"
	        crossorigin="anonymous"></script>
	<script type="text/javascript" src="form.js"></script>
</body>
</html>
