/**
 * Server POST request
 */
export function postData(url, data) {
	return fetch(`${window.app_server}/${url}`, {
		body: JSON.stringify(data),
		cache: 'no-cache',
		credentials: 'include',
		headers: {
			'User-Agent': window.app_title,
			'Accept': 'application/json',
			'Content-Type': 'application/json; charset=utf-8',
		},
		method: 'POST',
		mode: 'cors',
		referrer: 'no-referrer',
	})
	.then(response => response.json())
}

//https://stackoverflow.com/questions/47962519/html-special-character-symbol-not-rendering-in-react-component
export function decodeHTMLChars(strToDecode) {
	if (strToDecode === null || strToDecode === undefined) return "";
	const parser = new DOMParser();
	const decodedString = parser.parseFromString(`<!doctype html><body>${strToDecode}`, 'text/html').body.textContent;
	return decodedString;
}

export function loaderStart() {
	try {
		document.getElementById("loader").style.display = "block";
	} catch(e) {}
}
export function loaderEnd() {
	try {
		document.getElementById("loader").style.display = "none";
	} catch(e) {}
}

/**
 * Spreadsheet utilities - START
 */
// https://stackoverflow.com/questions/16245767/creating-a-blob-from-a-base64-string-in-javascript
export function b64toBlob(b64Data, contentType, sliceSize) {
	contentType = contentType || '';
	sliceSize = sliceSize || 512;
	//https://stackoverflow.com/questions/39490904/errorfailed-to-execute-atob-on-window-the-string-to-be-decoded-is-not-corr
	var byteCharacters = atob(b64Data.replace(/^data:application\/vnd\.openxmlformats-officedocument\.spreadsheetml\.sheet;base64,/, ''));
	var byteArrays = [];
	for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
		var slice = byteCharacters.slice(offset, offset + sliceSize);
		var byteNumbers = new Array(slice.length);
		for (var i = 0; i < slice.length; i++) {
			byteNumbers[i] = slice.charCodeAt(i);
		}
		var byteArray = new Uint8Array(byteNumbers);
		byteArrays.push(byteArray);
	}
	var blob = new Blob(byteArrays, {type: contentType});
	return blob;
}

//https://stackoverflow.com/questions/12504042/what-is-a-method-that-can-be-used-to-increment-letters
export function nextChar(c) {
	var u = c.toUpperCase();
	if (same(u,'Z')){
		var txt = '';
		var i = u.length;
		while (i--) {
			txt += 'A';
		}
		return (txt+'A');
	} else {
		var p = "";
		var q = "";
		if(u.length > 1){
			p = u.substring(0, u.length - 1);
			q = String.fromCharCode(p.slice(-1).charCodeAt(0));
		}
		var l = u.slice(-1).charCodeAt(0);
		var z = nextLetter(l);
		if(z==='A'){
			return p.slice(0,-1) + nextLetter(q.slice(-1).charCodeAt(0)) + z;
		} else {
			return p + z;
		}
	}
}
function nextLetter(l) {
	if(l<90) {
		return String.fromCharCode(l + 1);
	}
	else{
		return 'A';
	}
}
function same(str,char) {
	var i = str.length;
	while (i--) {
		if (str[i]!==char) {
			return false;
		}
	}
	return true;
}
// Spreadsheet utilities - END

/**
 * Settings utilities start
 */
export function setCookie(cname, cvalue, exdays) {
	var d = new Date();
	d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
	var expires = "expires="+d.toUTCString();
	document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}
export function getCookie(cname) {
	var name = cname + "=";
	var ca = document.cookie.split(';');
	for(var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) === ' ') {
			c = c.substring(1);
		}
		if (c.indexOf(name) === 0) {
			return c.substring(name.length, c.length);
		}
	}
	return "";
}
export function setBgImg(bgImg, bgImgStyle="zoomed") {
	let bdy = document.getElementsByTagName("BODY")[0];
	if (bgImg && bgImgStyle !== "none" && bgImgStyle !== "") {
		bdy.style.backgroundImage = "url('" + bgImg + "')";
	} else {
		bdy.style.backgroundImage = "none";
		return;
	}
	bdy.style.backgroundPosition = "center";
	bdy.style.backgroundRepeat = "no-repeat";
	switch(bgImgStyle) {
		case "centered":
			bdy.style.backgroundSize = "auto";
			break;
		case "tiled":
			bdy.style.backgroundSize = "auto";
			bdy.style.backgroundPosition = "0 0";
			bdy.style.backgroundRepeat = "repeat";
			break;
		case "stretched":
			bdy.style.backgroundSize = "100% 100%";
			break;
		case "scaled":
			bdy.style.backgroundSize = "contain";
			break;
		case "zoomed":
			bdy.style.backgroundSize = "cover";
			break;
		default:
			bdy.style.backgroundSize = "auto";
}
}
export function getBgImg() {
	let bgImg = document.getElementsByTagName("BODY")[0].style.backgroundImage;
	if (bgImg !== "none") {
		bgImg = bgImg.replace("url(\"", "");
		bgImg = bgImg.replace("url('", "");
		bgImg = bgImg.replace("\")", "");
		bgImg = bgImg.replace("')", "");
		return bgImg;
	} else {
		return "";
	}
}
/**
 * Using https://sightengine.com/docs/getstarted?signup=1
 */
export function setVarColors(obj, bgImg, bgImgStyle="zoomed") {

	if (!bgImg || bgImgStyle === "none") {
		setDefaultStyle();
		return;
	}

	postData("average_rgb.php", {
		url: bgImg
	})
	.then((data) => {
		if (!data.success) {
			setDefaultStyle();
			//TODO Show error popup
			obj.setState({ error: data.errors });
			/*Object.keys(data.errors).forEach(function(k) {
				console.log(data.errors[k]);
			});*/
		} else {

			let style = document.documentElement.style;

			if(data.rgb) {

				let rgb = data.rgb;
				let shadows = [0.25, 2, 0.6, 0.4, 2, 1.5, 2.4, 1.9, 1.2, 2.5];//max 2.5

				let maxHighlight = 255 / Math.max(rgb.r, rgb.g, rgb.b);

				if(Math.max(rgb.r, rgb.g, rgb.b) > 102) {//max of 102 is 2.5
					shadows = [maxHighlight*0.1, //0.25 / 2.5 = 0.1
						maxHighlight*0.8, maxHighlight*0.24,
						maxHighlight*0.16, maxHighlight*0.8, maxHighlight*0.6,
						maxHighlight*0.96, maxHighlight*0.76, maxHighlight*0.48, maxHighlight];
				}

				style.setProperty('--overlay', rgbaToString(rgb, 0.6, shadows[0]));
				style.setProperty('--navbarBgColor', rgbaToString(rgb, 0.5, shadows[1]));
				if(window.bgColorType) {
					style.setProperty('--bodyBg', rgbaToString(rgb, 1, shadows[2]));
				}
				style.setProperty('--panelBg', rgbaToString(rgb, 0.8, shadows[2]));
				style.setProperty('--panelHeaderFooterBg', rgbaToString(rgb, 0.8, shadows[3]));
				style.setProperty('--btnBg', rgbaToString(rgb, 0.8, shadows[4]));
				style.setProperty('--btnDarkerBg', rgbaToString(rgb, 0.8, shadows[5]));
				style.setProperty('--btnHoverBg', rgbaToString(rgb, 0.8, shadows[6]));
				style.setProperty('--btnActiveBg', rgbaToString(rgb, 0.8, shadows[7]));
				style.setProperty('--btnActiveFocusBg', rgbaToString(rgb, 0.8, shadows[8]));
				style.setProperty('--outline', rgbaToString(rgb, 0.5, shadows[9]));

				if(rgb.r > 90 && rgb.g > 90 && rgb.b > 90) {
					style.setProperty('--btnFont', '#222');
					style.setProperty('--title-color', '#ccc');
					style.setProperty('--saveColor', '#090');
					style.setProperty('--deleteColor', '#f55');
				}
				else {
					style.setProperty('--btnFont', '#fff');
					style.setProperty('--title-color', '#ccc');
					style.setProperty('--saveColor', '#5f5');
					style.setProperty('--deleteColor', '#f99');
				}

				//console.log(rgb);
			}
			else {
				setDefaultStyle();
			}
		}
	})
	.catch((error2) => {
		setDefaultStyle();
		console.log(error2);
	});

/*
	fetch("https://api.sightengine.com/1.0/check.json", {
		body: new URLSearchParams(`models=properties` +
			`&api_user=1103140429&api_secret=jQeSQxHmf2pdoJCWChKt` +
			`&url=${bgImg}`),
		cache: 'no-cache',
		headers: {
			'User-Agent': window.app_title
		},
		method: 'POST',
		mode: 'cors',
		referrer: 'no-referrer',
	})
	.then(response => response.json())
	.then((data) => {

		let style = document.documentElement.style;

		if(data.colors.dominant) {

			let rgb = data.colors.dominant;
			let shadows = [0.25, 2, 0.6, 0.4, 2, 1.5, 2.4, 1.9, 1.2, 2.5];//max 2.5

			let maxHighlight = 255 / Math.max(rgb.r, rgb.g, rgb.b);

			if(Math.max(rgb.r, rgb.g, rgb.b) > 102) {//max of 102 is 2.5
				shadows = [maxHighlight*0.1, //0.25 / 2.5 = 0.1
					maxHighlight*0.8, maxHighlight*0.24,
					maxHighlight*0.16, maxHighlight*0.8, maxHighlight*0.6,
					maxHighlight*0.96, maxHighlight*0.76, maxHighlight*0.48, maxHighlight];
			}

			style.setProperty('--overlay', rgbaToString(rgb, 0.6, shadows[0]));
			style.setProperty('--navbarBgColor', rgbaToString(rgb, 0.5, shadows[1]));
			style.setProperty('--panelBg', rgbaToString(rgb, 0.8, shadows[2]));
			style.setProperty('--panelHeaderFooterBg', rgbaToString(rgb, 0.8, shadows[3]));
			style.setProperty('--btnBg', rgbaToString(rgb, 0.8, shadows[4]));
			style.setProperty('--btnDarkerBg', rgbaToString(rgb, 0.8, shadows[5]));
			style.setProperty('--btnHoverBg', rgbaToString(rgb, 0.8, shadows[6]));
			style.setProperty('--btnActiveBg', rgbaToString(rgb, 0.8, shadows[7]));
			style.setProperty('--btnActiveFocusBg', rgbaToString(rgb, 0.8, shadows[8]));
			style.setProperty('--outline', rgbaToString(rgb, 0.5, shadows[9]));

			if(rgb.r > 90 && rgb.g > 90 && rgb.b > 90) {
				style.setProperty('--btnFont', '#222');
				style.setProperty('--title-color', '#ccc');
				style.setProperty('--saveColor', '#090');
				style.setProperty('--deleteColor', '#f55');
			}
			else {
				style.setProperty('--btnFont', '#fff');
				style.setProperty('--title-color', '#ccc');
				style.setProperty('--saveColor', '#5f5');
				style.setProperty('--deleteColor', '#f99');
			}

			//console.log(rgb);
		}
		else {
			setDefaultStyle();
		}
	})
	.catch((error2) => {
		setDefaultStyle();
		console.log(error2);
	});
	*/
}
function rgbaToString(rgb, a, shadow) {
	let r, g, b;
	r = rgb.r*shadow;
	g = rgb.g*shadow;
	b = rgb.b*shadow;
	return `rgba(${r},${g},${b},${a})`
}
function setDefaultStyle() {
	let style = document.documentElement.style;
	style.setProperty('--font-color', '#fff');
	style.setProperty('--title-color', '#ccc');
	style.setProperty('--overlay', 'rgba(0,0,0,0.6)');
	style.setProperty('--navbarBgColor', 'rgba(78,93,108, 0.5)');
	if(window.bgColorType) {
		style.setProperty('--bodyBg', 'rgba(43, 62, 80, 1)');
	}
	style.setProperty('--panelBg', 'rgba(40,40,50,0.8)');
	style.setProperty('--panelHeaderFooterBg', 'rgba(30,30,40,0.8)');
	style.setProperty('--btnBg', 'rgba(78,93,108,0.8)');
	style.setProperty('--btnFont', '#fff');
	style.setProperty('--btnDarkerBg', 'rgba(51,68,85,0.8)');
	style.setProperty('--btnHoverBg', 'rgba(72,85,99,0.8)');
	style.setProperty('--btnActiveBg', 'rgba(57,68,78,0.8)');
	style.setProperty('--btnActiveFocusBg', 'rgba(42,50,58,0.8)');
	style.setProperty('--outline', 'rgba(94, 158, 214,1)');
	style.setProperty('--saveColor', '#5f5');
	style.setProperty('--deleteColor', '#f99');
}

var player;
export function PlayYTMusic(id) {

	try {
		if (!id && player && typeof player === "object") { // No song selected but player exists
			if( player.src !== null) player.stopVideo();
			player.destroy();
			return;
		}
		else if (!id) {return;} // No song selected
	} catch(e) {
		//console.log(e);
	}

	try {
		if (player && typeof player === "object" &&
				getIDFromYTURL(player.getVideoUrl()) !== id) { // If song changed
			if( player.src !== null) player.stopVideo();
			player.destroy();
		} else if (player && typeof player === "object" &&
				getIDFromYTURL(player.getVideoUrl()) === id) { // Same song than the one already playing
			return;
		}
	} catch(e) {
		//console.log(e);
	}

	try {
		player = new window.YT.Player('player', {
			/**
			 * Not doing anything apparently...
			enablejsapi: 1,
			origin: window.location.protocol + "//" +  window.location.hostname,
			*/
			videoId: id, // this is the id of the video at youtube (the stuff after "?v=")
			playerVars: {
				autoplay: 1,
				loop: 1
			},
			events: {
				onReady: function (e) {
					//console.log('video is loaded');
					//e.target.playVideo(); // unnecessary because of autoplay
				},
				onStateChange: function (e) {
					/*
					if (e.data === window.YT.PlayerState.PLAYING) {
						//console.log('video started playing');
					}*/
					/**
					 * Loop parameter only works when used with playlist, so we need this
					 */
					if (e.data === window.YT.PlayerState.ENDED) {
						e.target.playVideo();
					}
				}
			}
		});
	} catch(e) {
		//console.log(e);
	}
}
//https://stackoverflow.com/questions/3452546/how-do-i-get-the-youtube-video-id-from-a-url
export function getIDFromYTURL(url){
	var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#&?]*).*/;
	var match = url.match(regExp);
	return (match&&match[7].length===11)? match[7] : false;
}
