<?php

/* Copyright (c) 2018 Insomnium Eye */

require_once 'inc/utilities.php';

cors();

$errors = array();
$data = array();

if (!isInstalled()) {
	$errors['not_installed'] = $app_title . ' server is not installed.';
}
elseif (!SessionManager::isLoggedIn()) {
	$errors['not_logged'] = 'You are not logged in!';
}
else {
	SessionManager::logout();
	//header("location: login.php");
}

if ( ! empty($errors)) {
	$data['errors']  = $errors;
	$data['success'] = false;
} else {
	$data['message'] = "Logout successful.";
	$data['success'] = true;
}

echo json_encode($data);

?>
