/* Copyright (c) 2018 Insomnium Eye */

/* Checked with http://jshint.com/ */
/* jshint node: true */
/* jshint esversion: 6 */
/* globals $:false */

/* Checked with http://www.jslint.com/ */
/*jslint white, node */
/*jslint single, node */
/*jslint this, node */
/*global $ */
/*jslint browser:true */

'use strict';

function activateTab(tab) {
	$('.nav-link').addClass('disabled');
	$('.nav-tabs a[href="#' + tab + '"]').removeClass('disabled');
	$('.nav-tabs a[href="#' + tab + '"]').tab('show');
}

$(document).ready(function() {

	// Initialize page stuff
	$(function() {
		$('[data-toggle="tooltip"]').tooltip();
	});

	// Create tables
	$('#remove-db').click(function(event) {

		$.ajax({
			type: 'POST',
			url: 'remove_db.php',
			data: '',
			dataType: 'json',
			encode: true
		}).done(function(data) {
			$('#remove-db + .alert-danger').remove();
			$('#remove-db + .alert-success').remove();
			if (!data.success) {
				let message = '';
				Object.keys(data.errors).forEach(function(k) {
					message += data.errors[k] + '<br />';
				});
				$('#database').append('<div class="alert alert-danger">' +
					message + '</div>');
			} else {
				$('#database').append('<div class="alert alert-success">' +
					data.message + '</div>');
				activateTab('config');
			}
		});/*.fail(function(data) { // Use for debug
			console.log(data);
		});*/
	});

	// Create configuration
	$('#remove-config').click(function(event) {
		$.ajax({
			type: 'POST',
			url: 'remove_config.php',
			data: '',
			dataType: 'json',
			encode: true
		}).done(function(data) {
			$('#remove-config + .alert').remove();
			if (!data.success) {
				let message = '';
				Object.keys(data.errors).forEach(function(k) {
					message += data.errors[k] + '<br />';
				});
				$('#config').append('<div class="alert alert-danger">' +
					message + '</div>');
			} else {
				$('#config').append('<div class="alert alert-success">' +
					data.message + '</div>');
				activateTab('done');
			}
		});
	});

});
