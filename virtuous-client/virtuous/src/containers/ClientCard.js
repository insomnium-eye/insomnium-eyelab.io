import React, { Component } from "react";
import { Col, Button, OverlayTrigger, Tooltip } from "react-bootstrap";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css' // Import css
import { faBuilding, faMapMarkerAlt,
	faPhone, faIdCardAlt, faTrashAlt,
	faCheck, faTimes, faEdit } from '@fortawesome/free-solid-svg-icons';
import { loaderStart, loaderEnd, postData, decodeHTMLChars } from "../utils.js";
import "./ClientCard.css";
//import avatar from '../images/default-avatar.jpg';

// Style: https://www.w3schools.com/howto/howto_css_team.asp
export default class ClientCard extends Component {

	constructor(props) {
		super(props);

		this.state = {
			editing: false,
			error: "",
			deleted: false
		};
	}

	render() {

		let error = <br key="br2" />;

		if (this.state.error) {
			Object.keys(this.state.error).forEach((k) => {
				error = [error, <div className="alert alert-danger" key={k}>
					{ this.state.error[k] }
				</div>]
			});
		}

		return (
			<Col sm={6} md={6} lg={4} style={ this.state.deleted ? {display:"none"} : {display:"block"}}>
				<div className="card">
					{/*img className="avatar-cover" src={avatar} alt="Jane" /> */ }
					<div className="container">
						<h2 ref={r => this.inputName = r}
							contentEditable={ this.state.editing ? true : false }
							suppressContentEditableWarning={true}
							data-placeholder="Insert name...">
							{decodeHTMLChars(this.props.name)}
						</h2>
						<OverlayTrigger placement="left" overlay={(
							<Tooltip id="editTooltip">
								Edit
							</Tooltip>
						)}>
							<Button
								className="editButton"
								type="button"
								onClick={this.editButton}
								style={ this.state.editing ? {display:"none"} : {display:"block"}}
								ref={r => this.editRef = r}>
									<FontAwesomeIcon icon={faEdit} />
							</Button>
						</OverlayTrigger>
						<p className="title"
							data-placeholder="Insert profession..."
							ref={r => this.inputProfession = r}
							suppressContentEditableWarning={true}
							contentEditable={ this.state.editing ? true : false }>
							{decodeHTMLChars(this.props.profession)}
						</p>
						<details open={ this.state.editing ? "open" : "" }
							style={{marginBottom: "15px"}}>
							<summary style={{outline: "0"}}>View more...</summary>
							<table className="clientDetails">
								<tbody>
									<tr>
										<td>
											<OverlayTrigger placement="top" overlay={(
												<Tooltip id="companyTooltip">
													Company
												</Tooltip>
											)}>
												<FontAwesomeIcon icon={faBuilding} />
											</OverlayTrigger>
										</td>
										<td ref={r => this.inputCompany = r}
											suppressContentEditableWarning={true}
											contentEditable={ this.state.editing ? true : false }
											data-placeholder="Insert company...">
											{decodeHTMLChars(this.props.company)}
										</td>
										<td>
											<OverlayTrigger placement="top" overlay={(
												<Tooltip id="locationTooltip">
													Location
												</Tooltip>
											)}>
												<FontAwesomeIcon icon={faMapMarkerAlt} />
											</OverlayTrigger>
										</td>
										<td data-placeholder="Insert address..."
											ref={r => this.inputAddress = r}
											suppressContentEditableWarning={true}
											contentEditable={ this.state.editing ? true : false }>
											{decodeHTMLChars(this.props.address)}
										</td>
									</tr>
									<tr>
										<td>
											<OverlayTrigger placement="top" overlay={(
												<Tooltip id="licenseTooltip">
													License Number
												</Tooltip>
											)}>
												<FontAwesomeIcon icon={faIdCardAlt} />
											</OverlayTrigger>
										</td>
										<td data-placeholder="Insert license..."
											ref={r => this.inputLicense = r}
											suppressContentEditableWarning={true}
											contentEditable={ this.state.editing ? true : false }>
											{decodeHTMLChars(this.props.license_no)}
										</td>
										<td>
											<OverlayTrigger placement="top" overlay={(
												<Tooltip id="phoneTooltip">
													Phone
												</Tooltip>
											)}>
												<FontAwesomeIcon icon={faPhone} />
											</OverlayTrigger>
										</td>
										<td data-placeholder="Insert phone..."
											ref={r => this.inputPhone = r}
											suppressContentEditableWarning={true}
											contentEditable={ this.state.editing ? true : false }>
											{decodeHTMLChars(this.props.phone)}
										</td>
									</tr>
								</tbody>
							</table>
							<p>
								<a ref={r => this.inputEmail = r}
									suppressContentEditableWarning={true}
									contentEditable={ this.state.editing ? true : false }
									data-placeholder="Insert email..."
									href={"mailto:" + decodeHTMLChars(this.props.email)}>
									{decodeHTMLChars(this.props.email)}
								</a>
							</p>
							<p ref={r => this.inputNotes = r}
								suppressContentEditableWarning={true}
								contentEditable={ this.state.editing ? true : false }
								className="clientNotes"
								data-placeholder="Insert notes...">
								{decodeHTMLChars(this.props.notes)}
							</p>
						</details>
						<p style={ this.state.editing ? {display:"block"} : {display:"none"}}
							ref={r => this.updateButtons = r}>
							<OverlayTrigger placement="top" overlay={(
								<Tooltip id="saveTooltip">
									Save
								</Tooltip>
							)}>
								<Button className="saveBtn" style={{marginRight:"15px"}}
									type="button"
									onClick={this.updateClient}>
									<FontAwesomeIcon icon={faCheck} />
								</Button>
							</OverlayTrigger>
							<OverlayTrigger placement="top" overlay={(
								<Tooltip id="cancelTooltip">
									Cancel
								</Tooltip>
							)}>
								<Button type="button" className="cancelBtn"
									onClick={this.cancelEdit}>
									<FontAwesomeIcon icon={faTimes} />
								</Button>
							</OverlayTrigger>
							<OverlayTrigger placement="top" overlay={(
								<Tooltip id="deleteTooltip">
									Delete
								</Tooltip>
							)}>
								<Button type="button" className="trashBtn"
									style={{float:"right"}}
									onClick={this.deleteClient}>
									<FontAwesomeIcon icon={faTrashAlt} />
								</Button>
							</OverlayTrigger>
						</p>
					</div>
				</div>
				{ error }
			</Col>
		);
	}

	editButton = event => {
		this.setState({editing: true});
	}

	cancelEdit = event => {
		this.setState({editing: false});
		this.inputName.innerText = this.props.name;
		this.inputProfession.innerText = this.props.profession;
		this.inputCompany.innerText = this.props.company;
		this.inputAddress.innerText = this.props.address;
		this.inputLicense.innerText = this.props.license_no;
		this.inputPhone.innerText = this.props.phone;
		this.inputEmail.innerText = this.props.email;
		this.inputNotes.innerText = this.props.notes;
	}

	updateClient = event => {

		loaderStart();

		postData("clients.php", {
			action: "update",
			cid: this.props.cid,
			name: this.inputName.innerText,
			profession: this.inputProfession.innerText,
			company: this.inputCompany.innerText,
			address: this.inputAddress.innerText,
			license_no: this.inputLicense.innerText,
			phone: this.inputPhone.innerText,
			email: this.inputEmail.innerText,
			notes: this.inputNotes.innerText
		})
		.then((data) => {
			if (!data.success) {
				this.setState({ error: data.errors });
			} else {
				this.setState({ error: "" });
				this.setState({editing: false});
				this.inputEmail.href = "mailto:" + decodeHTMLChars(this.inputEmail.innerText);
			}
			loaderEnd();
		})
		.catch((error2) => {
			this.setState({ error: { invalid: 'Error: ' + error2 } });
			console.log(error2);
			loaderEnd();
		});
	}


	deleteClient = event => {

		confirmAlert({
			title: 'Delete client?',
			message: `Are you sure you want to delete client ${this.inputName.innerText}?`,
			buttons: [
				{
					label: 'Yes',
					onClick: () => {

						loaderStart();

						postData("clients.php", {
							action: "delete",
							cid: this.props.cid
						})
						.then((data) => {
							if (!data.success) {
								this.setState({ error: data.errors });
							} else {
								this.setState({ error: "", editing: false, deleted: true });
							}
							loaderEnd();
						})
						.catch((error2) => {
							this.setState({ error: { invalid: 'Error: ' + error2 } });
							console.log(error2);
							loaderEnd();
						});

					}
				},
				{
					label: 'No'
				}
			]
		});
	}


}
