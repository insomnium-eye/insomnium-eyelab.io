-- Insomnium-Eye
-- https://insomnium-eye.myvnc.com
-- Copyright (c) 2018 Insomnium Eye
--
-- Authors:
-- - David G. Piper
-- - David Gonçalves
--
-- Database: `Virtuous_Assistant`
-- Naming convention: under_scores

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

-- --------------------------------------------------------

--
-- Table structure of `users`
--

CREATE TABLE IF NOT EXISTS `users` (
	`uid` int NOT NULL AUTO_INCREMENT,
	`username` varchar(50) NOT NULL,
	`password` varchar(255) NOT NULL,
	`admin` tinyint(1) NOT NULL DEFAULT 0,
	-- https://stackoverflow.com/questions/5133580/which-mysql-datatype-to-use-for-an-ip-address
	`last_ipv4` int unsigned DEFAULT NULL,
	`last_ipv6` binary(16) DEFAULT NULL,
	`created_by` int DEFAULT NULL,
	`created_at` datetime DEFAULT CURRENT_TIMESTAMP,
	`deleted` boolean DEFAULT FALSE,
	PRIMARY KEY (`uid`),
	UNIQUE (`username`),
	FOREIGN KEY (`created_by`) REFERENCES `users`(`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure of `clients`
--

CREATE TABLE IF NOT EXISTS `clients` (
	`cid` int NOT NULL AUTO_INCREMENT,
	`name` varchar(255) DEFAULT NULL,
	`email` varchar(255) DEFAULT NULL,
	`phone` varchar(20) DEFAULT NULL,
	`address` varchar(255) DEFAULT NULL,
	`company` varchar(255) DEFAULT NULL,
	`profession` varchar(255) DEFAULT NULL,
	`license_no` varchar(30) DEFAULT NULL COMMENT 'Client''s driver''s license number.',
	`notes` text DEFAULT NULL,
	`created_by` int DEFAULT NULL,
	`created_at` datetime DEFAULT CURRENT_TIMESTAMP,
	`deleted` boolean DEFAULT FALSE,
	PRIMARY KEY (`cid`),
	UNIQUE (`license_no`),
	FOREIGN KEY (`created_by`) REFERENCES `users`(`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure of `vehicles`
--

CREATE TABLE IF NOT EXISTS `vehicles` (
	`vid` int NOT NULL AUTO_INCREMENT,
	`plate` varchar(20) NOT NULL,
	`created_by` int DEFAULT NULL,
	`created_at` datetime DEFAULT CURRENT_TIMESTAMP,
	`deleted` boolean DEFAULT FALSE,
	PRIMARY KEY (`vid`),
	UNIQUE (`plate`),
	FOREIGN KEY (`created_by`) REFERENCES `users`(`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure of `spots`
--

CREATE TABLE IF NOT EXISTS `spots` (
	`sid` int NOT NULL AUTO_INCREMENT,
	`number` int NOT NULL COMMENT 'Parking Spot Number',
	`size` varchar(10) DEFAULT NULL,
	`cost` decimal(3,2) DEFAULT '0',
	`pay_type` varchar(20) DEFAULT NULL,
	`created_by` int DEFAULT NULL,
	`created_at` datetime DEFAULT CURRENT_TIMESTAMP,
	`deleted` boolean DEFAULT FALSE,
	PRIMARY KEY (`sid`),
	FOREIGN KEY (`created_by`) REFERENCES `users`(`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure of `clients_vehicles`
--

CREATE TABLE IF NOT EXISTS `clients_vehicles` (
	`cid` int NOT NULL,
	`vid` int NOT NULL,
	`from` datetime DEFAULT NULL COMMENT 'When client acquired vehicle',
	`to` datetime DEFAULT NULL COMMENT 'When client disowned vehicle',
	`created_by` int DEFAULT NULL,
	`created_at` datetime DEFAULT CURRENT_TIMESTAMP,
	`deleted` boolean DEFAULT FALSE,
	FOREIGN KEY (`cid`) REFERENCES `clients`(`cid`),
	FOREIGN KEY (`vid`) REFERENCES `vehicles`(`vid`),
	FOREIGN KEY (`created_by`) REFERENCES `users`(`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure of `vehicles_spots`
--

CREATE TABLE IF NOT EXISTS `vehicles_spots` (
	`sid` int NOT NULL,
	`vid` int NOT NULL,
	`from` datetime DEFAULT NULL COMMENT 'Vehicle Move-In Date.',
	`to` datetime DEFAULT NULL COMMENT 'Vehicle Move-Out Date.',
	`created_by` int DEFAULT NULL,
	`created_at` datetime DEFAULT CURRENT_TIMESTAMP,
	`deleted` boolean DEFAULT FALSE,
	FOREIGN KEY (`sid`) REFERENCES `spots`(`sid`),
	FOREIGN KEY (`vid`) REFERENCES `vehicles`(`vid`),
	FOREIGN KEY (`created_by`) REFERENCES `users`(`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
