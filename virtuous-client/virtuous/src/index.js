import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import './css/bootstrap.min.css';
import Login from "./containers/Login";
import registerServiceWorker from './registerServiceWorker';

window.app_title = "The Rich Group";
window.app_server = "";
document.title = window.app_title;

ReactDOM.render(
	<Login />,
	document.getElementById("root")
);
/*
ReactDOM.render(
	<Router>
		<App />
	</Router>,
	document.getElementById("root")
);*/
registerServiceWorker();
