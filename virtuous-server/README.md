## Install

### Requirements

* Apache
* MySQL
* PHP (w/ php-mysql)
    * php-imagick - for changing elements colors when setting background image.
	
	  ```sh
	  sudo apt-get install php-imagick
	  sudo systemctl restart apache2
	  ```
* HTTPS (essential for security concerns)

### Install

1. chmod all directories 755 and files 644.
    Except inc/config.php which should be chmod 777 at first.

2. Create a database and database user for the application. Example:

   ```sql
   CREATE DATABASE virtuous_db;
   CREATE USER 'virtuous_user'@'localhost' IDENTIFIED BY 'virtuous_pw';
   GRANT ALL PRIVILEGES ON virtuous_db . * TO 'virtuous_user'@'localhost';
   FLUSH PRIVILEGES;
   ```

3. Run install script by going to http(s)://your-url/virtuous-server/install

4. chmod 400 inc/config.php

5. Remove `install` and `uninstall` directories or place a `.htaccess` file in them with `deny from all` so that they become inaccessible.


_Note: Uninstall directory exists for development purposes only. This directory should not be present in a production environment._

### Debug

For debugging php files, we may include the following code on top of them:
```php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

ini_set("log_errors", 1);
ini_set("error_log", "/var/www/html/virtuous/virtuous-server/php-error.log");
error_log( "Hello, errors!" );
```
The log file should have 777 permissions or it won't be written.
Don't forget to remove this code for release!

### Inspiration

**Login:**
- https://www.techiediaries.com/php-rest-api/
- https://www.tutorialrepublic.com/php-tutorial/php-mysql-login-system.php
- https://www.formget.com/login-form-in-php/
- https://www.formget.com/login-form-in-php/

**CORS Security Risks**
- https://mobilejazz.com/blog/which-security-risks-do-cors-imply/
- https://www.gracefulsecurity.com/html5-cross-origin-resource-sharing-cors-vulnerabilities/

**PHP Session Security**
- http://blog.teamtreehouse.com/how-to-create-bulletproof-sessions
- https://stackoverflow.com/questions/6483092/php-session-hijacking
- https://www.simonholywell.com/post/2013/05/improve-php-session-cookie-security/
- https://security.stackexchange.com/questions/157133/is-a-secure-cookie-without-the-httponly-flag-a-problem

**SQL Best Practices**
- https://www.upwork.com/hiring/data/tips-and-best-practices-for-sql-programmers/


---

_Copyright &copy; 2018 Insomnium Eye_