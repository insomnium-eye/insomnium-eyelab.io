<?php

/* Copyright (c) 2018 Insomnium Eye */

require_once 'inc/dbclass.php';
require_once 'inc/utilities.php';

cors();

$errors = array();
$data = array();

if (!isInstalled()) {
	$errors['not_installed'] = $app_title . ' server is not installed.';
}
elseif (!SessionManager::isLoggedIn()) {
	$errors['not_logged'] = 'You are not logged in!';
}

$valid_actions = ["list", "insert", "update", "delete"];
$action = "";
$json = "";
$sort = "created_at DESC";// Do not put user input on the $sort value!
$searchOption = $searchText = "";
$cid = $name = $profession = $company = $address = $license_no = $email = $phone = $notes = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
	$json = json_decode(file_get_contents('php://input'), true);
	$action = trim($json['action']);
	if (empty($action)) {
		$errors['action'] = 'No action defined.';
	}
	elseif (! in_array($action, $valid_actions)) {
		$errors['action'] = 'Invalid action.';
	}
	elseif ($action === "list") {
		$sort_ = (!empty(trim($json['sort'])) ? trim($json['sort']) : "");
		$order_ = (!empty(trim($json['order'])) ? trim($json['order']) : "");
		// Do not put unvalidated user input on the $sort value!
		if (!empty($sort_) && ($sort_ === "name" || $sort_ === "created_at")) {
			$sort = $sort_;
		}
		if (!empty($sort_) && !empty($order_) && ($order_ === "asc" || $order_ === "desc")) {
			$sort .= " " . $order_;
		}
		$searchOption_ = (!empty(trim($json['searchOption'])) ? trim($json['searchOption']) : "");
		$searchText_ = (!empty(trim($json['searchText'])) ? trim($json['searchText']) : "");
		// Do not put unvalidated user input on the $searchOption value!
		$options = array("name", "profession", "company", "address", "license_no", "phone", "email", "notes");
		if (!empty($searchOption_) && in_array($searchOption_, $options)) {
			$searchOption = $searchOption_;
		}
		if (!empty($searchText_)) {
			$searchText = $searchText_;
		}
	}
	elseif ($action === "insert" || $action === "update") {
		// The app already gives us encoded special chars, so encoding again would cause the &amp;amp; issue
		// So, in order to prevent html injection, we decode what the app sends us and encode again.
		if(!empty($json['cid'])) $cid = (int) trim($json['cid']);
		if(!empty($json['name'])) $name = htmlspecialchars(htmlspecialchars_decode(trim($json['name'])));
		if(!empty($json['profession'])) $profession = htmlspecialchars(htmlspecialchars_decode(trim($json['profession'])));
		if(!empty($json['company'])) $company = htmlspecialchars(htmlspecialchars_decode(trim($json['company'])));
		if(!empty($json['address'])) $address = htmlspecialchars(htmlspecialchars_decode(trim($json['address'])));
		if(!empty($json['license_no'])) $license_no = htmlspecialchars(htmlspecialchars_decode(trim($json['license_no'])));
		if(!empty($json['email'])) $email = htmlspecialchars(htmlspecialchars_decode(trim($json['email'])));
		if(!empty($json['phone'])) $phone = htmlspecialchars(htmlspecialchars_decode(trim($json['phone'])));
		if(!empty($json['notes'])) $notes = htmlspecialchars(htmlspecialchars_decode(trim($json['notes'])));
		if ($action === "update" &&
			(empty($cid) || filter_var($cid, FILTER_VALIDATE_INT) == false)) { // Don't use === on filter_var
			$errors['cid_invalid'] = "Invalid client ID.";
		}
		if (empty($name)) {
			$errors['name_empty'] = "Name field cannot be empty.";
		}
	}
	elseif ($action === "delete") {
		if(!empty($json['cid'])) $cid = (int) trim($json['cid']);
		if (empty($cid) || filter_var($cid, FILTER_VALIDATE_INT) == false) { // Don't use === on filter_var
			$errors['cid_invalid'] = "Invalid client ID.";
		}
	}
} else {
	$errors['post'] = 'Must send data over POST request method.';
}

if (empty($errors)) {
	try {
		$dbclass = new DBClass();
		$conn = $dbclass->getConnection();

		if ($action === "list") {
			$stmt = $conn->prepare(" SELECT cid, name, email, phone, address, company,
					profession, license_no, notes
				FROM clients
				WHERE deleted=0 " .
				(!empty($searchText) && !empty($searchOption) ? " AND " . $searchOption . " LIKE :searchText " : "") .
				" ORDER BY $sort "); // Do not put unvalidated user input on the $searchOption and $sort values!

			$searchText = '%' . $searchText . '%';
			$stmt->bindParam(':searchText', $searchText);

			$stmt->execute();

			$result = $stmt->fetchAll(PDO::FETCH_ASSOC);

			$data['message'] = $result;
		}
		elseif ($action === "insert") {

			$cidTmp = null;

			// First check if an user with that license already exists and is deleted,
			// So we update instead of deleting
			if (!empty($license_no)) {
				$stmt = $conn->prepare(" SELECT cid
				FROM clients
				WHERE license_no = :license_no AND deleted = 1 ");

				$stmt->bindParam(':license_no', $license_no);

				$stmt->execute();

				$row = $stmt->fetch();

				if (isset($row["cid"])) {
					$cidTmp = $row["cid"];
				}
			}

			if (!empty($license_no) && !is_null($cidTmp)) {

				$stmt = $conn->prepare(" UPDATE clients
				SET name = :name, profession = :profession, company = :company,
					address = :address, license_no = :license_no, email = :email,
					phone = :phone, notes = :notes, created_at = NOW(),
					created_by = :uid, deleted = 0
				WHERE cid = :cidTmp ");

				$uid = (int) $_SESSION['uid'];
				$stmt->bindParam(':uid', $uid, PDO::PARAM_INT);
				$stmt->bindParam(':cidTmp', $cidTmp, PDO::PARAM_INT);

				bindAllFields($stmt);

				$stmt->execute();

			} else {

				$stmt = $conn->prepare(" INSERT INTO clients (name, profession, company,
					address, license_no, email, phone, notes, created_by)
				VALUES (:name, :profession, :company, :address, :license_no,
					:email, :phone, :notes, :uid) ");

				$uid = (int) $_SESSION['uid'];
				$stmt->bindParam(':uid', $uid, PDO::PARAM_INT);

				bindAllFields($stmt);

				$stmt->execute();

			}

			$data['message'] = "Insert successful.";
		}
		elseif ($action === "update" && !empty($cid) && filter_var($cid, FILTER_VALIDATE_INT) == true) {// Don't use === on filter_var

			$cidTmp = null;

			// First check if an user with that license already exists and is deleted,
			// So we permanently delete the deleted client so that we can update the new one
			// which supposedly is the same client (since it has the same license number)
			if (!empty($license_no)) {
				$stmt = $conn->prepare(" SELECT cid
				FROM clients
				WHERE license_no = :license_no AND deleted = 1 ");

				$stmt->bindParam(':license_no', $license_no);

				$stmt->execute();

				$row = $stmt->fetch();

				if (isset($row["cid"])) {
					$cidTmp = $row["cid"];
				}
			}

			if (!empty($license_no) && !is_null($cidTmp)) {

				$stmt = $conn->prepare(" DELETE FROM clients
					WHERE cid = :cidTmp ");

				$stmt->bindParam(':cidTmp', $cidTmp, PDO::PARAM_INT);

				$stmt->execute();

			}


			$stmt = $conn->prepare(
				" UPDATE clients
				  SET name = :name, profession = :profession, company = :company,
				      address = :address, license_no = :license_no, email = :email,
					  phone = :phone, notes = :notes
				  WHERE cid = :cid ");

			bindAllFields($stmt);

			$stmt->execute();

			$data['message'] = "Update successful.";
		}
		elseif ($action === "delete" && !empty($cid) && filter_var($cid, FILTER_VALIDATE_INT) == true) {// Don't use === on filter_var
			$stmt = $conn->prepare(
				" UPDATE clients
				  SET deleted = 1
				  WHERE cid = :cid ");

			$stmt->bindParam(':cid', $cid, PDO::PARAM_INT);

			$stmt->execute();

			$data['message'] = "Delete successful.";
		}
		else {
			$errors['action'] = 'Invalid action.';
		}

	}
	catch(PDOException $e) {
		$msg =  $e->getMessage();
		if ($e->errorInfo[1] == 1062 && ($action === "insert" || $action === "update")
			&& strpos($msg, 'license_no') !== false) {
			$errors['exception'] = "A client with this license number already exists.";
		} else {
			$errors['exception'] = $msg;
		}
	}
	$dbclass->closeConnection();
}

if ( ! empty($errors)) {
	$data['errors']  = $errors;
	$data['success'] = false;
} else {
	$data['success'] = true;
}

echo json_encode($data);

/* Helper functions */
// We pass the value by reference so it is modified outside the function
function bindAllFields(&$stmt) {

	global $cid, $name, $profession, $company, $address, $license_no, $email, $phone, $notes;

	if (!empty($cid)) $stmt->bindParam(':cid', $cid, PDO::PARAM_INT);
	(empty($name) ? $stmt->bindValue(':name', null) : $stmt->bindParam(':name', $name) );
	(empty($profession) ? $stmt->bindValue(':profession', null) : $stmt->bindParam(':profession', $profession) );
	(empty($company) ? $stmt->bindValue(':company', null) : $stmt->bindParam(':company', $company) );
	(empty($address) ? $stmt->bindValue(':address', null) : $stmt->bindParam(':address', $address) );
	(empty($license_no) ? $stmt->bindValue(':license_no', null) : $stmt->bindParam(':license_no', $license_no) );
	(empty($email) ? $stmt->bindValue(':email', null) : $stmt->bindParam(':email', $email) );
	(empty($phone) ? $stmt->bindValue(':phone', null) : $stmt->bindParam(':phone', $phone) );
	(empty($notes) ? $stmt->bindValue(':notes', null) : $stmt->bindParam(':notes', $notes) );
}

?>
