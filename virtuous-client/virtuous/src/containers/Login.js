import React, { Component } from "react";
import ReactDOM from 'react-dom';
import { MemoryRouter } from "react-router-dom";
import App from '../App';
import { Button, FormGroup, FormControl, ControlLabel, Modal,
	OverlayTrigger, Tooltip, Radio } from "react-bootstrap";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCog, faExclamationTriangle, faInfoCircle } from '@fortawesome/free-solid-svg-icons';
import "./Login.css";
import Preloader from '../Preloader';
import { loaderStart, loaderEnd, postData, setBgImg, setVarColors,
	PlayYTMusic, getIDFromYTURL, setCookie, getCookie, getBgImg } from "../utils.js";

export default class Login extends Component {

	constructor(props) {
		super(props);

		let server = "https://mangad.ddns.net/virtuous-server/";
		//"http://localhost/virtuous/virtuous-server";
		let bgImg = "";
		let bgImgStyle = "zoomed";
		let ytMus = "";
		let bgColorType = true;// auto
		let bgColor = "#2b3e50";

		if (getCookie("server")) {
			server = getCookie("server");
		}
		window.app_server = server; // Used by setVarColors

		/* Set bg color before bg image */
		if (getCookie("bgColorType")) {
			bgColorType = (getCookie("bgColorType") === 'true');
		}
		window.bgColorType = bgColorType;
		if (getCookie("bgColor")) {
			bgColor = getCookie("bgColor");
		}
		if(!bgColorType) {
			document.documentElement.style.setProperty('--bodyBg', bgColor);
		}

		if (getCookie("bgImg")) {
			bgImg = getCookie("bgImg");
			if (getCookie("bgImgStyle")) {
				bgImgStyle = getCookie("bgImgStyle");
			}
			setBgImg(getCookie("bgImg"), bgImgStyle);
			setVarColors(this, getCookie("bgImg"), bgImgStyle);
		}
		if (!getCookie("bgImg") && getBgImg()) {
			setVarColors(this, getBgImg(), bgImgStyle);
		}
		if (getCookie("ytMus")) {
			ytMus = getCookie("ytMus");
		}

		this.state = {
			server: server,
			inputServer: server,
			bgImg: bgImg,
			bgImgStyle: bgImgStyle,
			bgColorType: bgColorType,
			bgColor: bgColor,
			ytMus: ytMus,
			user: "",
			password: "",
			success: "",
			error: "",
			settingsShow: false
		};
	}

	componentWillMount() {

		// Check if already logged in
		window.app_server = this.state.server;
		postData("login.php", {})
		.then((data) => {
			if (!data.success) {
				if (data.errors.already_logged === true) {
					ReactDOM.render(
						<MemoryRouter>
							<App username={data.username} uid={data.uid} />
						</MemoryRouter>,
						document.getElementById("root")
					);
				}
			}
		})
		.catch((error2) => {});
	}

	componentDidMount() {

		// Play YouTube music
		if(getCookie("ytMus")) {
			PlayYTMusic(getIDFromYTURL(getCookie("ytMus")));
		}

		// Remove preloader
		setTimeout(() => {
			try {
				const ele = document.getElementById('preloader')
				if(ele) {
					// fade out
					ele.classList.add('available')
					setTimeout(() => {
						// remove from DOM
						try {
							ele.outerHTML = ''
						} catch (e) {}
					}, 700)
				}
			} catch (e) {}
		}, 2000);
	}

	validateForm() {
		return this.state.server.length > 0 &&
		       this.state.user.length > 0 &&
		       this.state.password.length >= 8;
	}

	handleChange = event => {
		this.setState({
			[event.target.id]: event.target.value
		});
	}

	handleSubmit = event => {
		this.loginDisable();
		loaderStart();
		window.app_server = this.state.server;
		postData("login.php", {
			username: this.state.user,
			password: this.state.password,
		})
		.then((data) => {
			if (!data.success) {
				this.setState({ error: data.errors });
				this.setState({ success: '' });
			} else {
				this.setState({ error: '' });
				this.setState({ success: data.message });
				ReactDOM.render(
					/**
					 * https://stackoverflow.com/questions/36505404/how-to-use-react-router-with-electron
					 * The (current) react-router docs say:
					 * Generally speaking, you should use a <BrowserRouter> if
					 * you have a server that responds to requests and a <HashRouter>
					 * if you are using a static file server.
					 *
					 * An Electron app is basically a static file server.
					 *
					 * MemoryRouter can also work, so long as all routing originates
					 * from within the React part of the app. It only falls down when
					 * you want to navigate to a specific page from the Browser process,
					 * e.g. you want to pop up a new window and navigate directly to a
					 * "General Preferences" page. In that case, you can do this with HashRouter:
					 *
					 * prefsWindow.loadURL(`file://${__dirname}/app/index.html#/general-prefs`);
					 * I don't think there is a way to do that with MemoryRouter (from the Browser process).
					 */
					<MemoryRouter>
						<App username={data.username} uid={data.uid} />
					</MemoryRouter>,
					document.getElementById("root")
				);
			}
			this.loginEnable();
			loaderEnd();
		})
		.catch((error2) => {
			this.setState({ error: { invalid: 'Error. Please check if server is valid.' } });
			this.setState({ success: '' });
			console.log(error2);
			this.loginEnable();
			loaderEnd();
		});
		event.preventDefault();
	}

	render() {

		let success = <br key="br1" />;
		let error = <br key="br2" />;
		let user_err = false;
		let password_err = false;

		if (this.state.success) {
			success = [success, <div className="alert alert-success" key="success">
				{ this.state.success }
			</div>];
		}
		if (this.state.error) {
			Object.keys(this.state.error).forEach((k) => {
				if(k === "user") {
					user_err = this.state.error[k];
				}
				else if(k === "pwd") {
					password_err = this.state.error[k];
				}
				else {
					error = [error, <div className="alert alert-danger" key={k}>
						{ this.state.error[k] }
					</div>]
				}
			});
		}

		return (
			<div className="Login">
				<div id="player" style={{position: "absolute", top: "-9999px", left: "-9999px"}}></div>
				<Preloader />
				<h1>{ window.app_title }</h1>
				<form onSubmit={this.handleSubmit} method="post">
					{/*<FormGroup controlId="server" bsSize="large">
						<ControlLabel>Server</ControlLabel>
						<FormControl
							autoFocus
							type="text"
							value={this.state.server}
							onChange={this.handleChange}
							required
						/>
					</FormGroup>*/}
					<FormGroup className={ user_err ? ("has-error has-feedback") : null }
							controlId="user" bsSize="large">
						<ControlLabel>User</ControlLabel>
						<FormControl
							autoFocus
							type="text"
							value={this.state.user}
							onChange={this.handleChange}
							required
						/>
						{ user_err ?
							(
								<span className="glyphicon glyphicon-remove
								form-control-feedback"></span>
							) : null }
						{ user_err ?
							(
								<span className="help-block">{user_err}</span>
							) : null }
					</FormGroup>
					<FormGroup className={ password_err ? ("has-error has-feedback") : null }
							controlId="password" bsSize="large">
						<ControlLabel>Password</ControlLabel>
						<FormControl
							value={this.state.password}
							onChange={this.handleChange}
							type="password"
							required
						/>
						{ password_err ?
							(<span className="glyphicon glyphicon-remove
								form-control-feedback"></span>) : null }
						{ password_err ?
							(
								<span className="help-block">{password_err}</span>
							) : null }
					</FormGroup>
					<Button
						id="loginBtn"
						block
						bsSize="large"
						disabled={!this.validateForm()}
						type="submit"
					>
						Login
					</Button>
					<div id="loader" className="loader"></div>
				</form>
				{ error }
				{ success }
				<Button
					id="settingsBtn"
					type="button"
					onClick={this.handleShow}
				>
					<FontAwesomeIcon icon={faCog} spin/>
				</Button>
				<Modal show={this.state.settingsShow} onHide={this.handleClose}>
					<Modal.Header closeButton>
						<Modal.Title>Settings</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<p>
							<strong>Local settings:</strong>
							<OverlayTrigger placement="top" overlay={(
								<Tooltip id="localInfo">
									These settings are <strong>not</strong> saved across multiple devices.
									For resetting them to default clear your browser cookies.
								</Tooltip>
							)}>
								<FontAwesomeIcon icon={faInfoCircle}
									style={{color: "#acf", marginLeft: "10px", marginBottom: "-1px"}} />
							</OverlayTrigger>
						</p>
						<FormGroup controlId="server">
							<ControlLabel>Server address:</ControlLabel>
							<FormControl
								onChange={e => this.setState({ inputServer: e.target.value })}
								type="text"
								value={this.state.inputServer}
								placeholder="Server address..."
							/>
						</FormGroup>
						<FormGroup controlId="bgImg">
							<ControlLabel>Background image URL:</ControlLabel>
							<FormControl
								onChange={e => this.setState({ bgImg: e.target.value })}
								type="text"
								value={this.state.bgImg}
								placeholder="Background image..."
							/>
						</FormGroup>
						<FormGroup controlId="bgImgStyle">
							<ControlLabel>Background style:</ControlLabel>
							<FormControl componentClass="select" placeholder="style"
								value={this.state.bgImgStyle}
								onChange={e => this.setState({ bgImgStyle: e.target.value })}>
								<option value="none">None</option>
								<option value="centered">Centered</option>
								<option value="tiled">Tiled</option>
								<option value="stretched">Stretched</option>
								<option value="scaled">Scaled</option>
								<option value="zoomed">Zoomed</option>
							</FormControl>
						</FormGroup>
						<FormGroup controlId="bgColor">
							<ControlLabel>Background color:</ControlLabel><br />
							<Radio name="radioColor"
								defaultChecked={this.state.bgColorType}
								onChange={e => this.setState({ bgColorType: e.target.checked })}
								inline>
								Auto
							</Radio>{' '}
							<Radio name="radioColor"
								defaultChecked={!this.state.bgColorType}
								onChange={e => this.setState({ bgColorType: !e.target.checked })}
								inline>
								Color
							</Radio>{' '}
							<input type="color" id="head" name="color"
								value={this.state.bgColor}
								onChange={e => this.setState({ bgColor: e.target.value })} />
						</FormGroup>
						<FormGroup controlId="ytMus">
							<ControlLabel>Login music (YouTube URL):
								<OverlayTrigger placement="top" overlay={(
									<Tooltip id="YTTooltip">
										Autoplay is disabled in Chrome by default &mdash; music
										will not play upon opening the app. If you wish
										to use this feature you must go to <a href="chrome://flags/#autoplay-policy">
										chrome://flags/#autoplay-policy</a> and change the "Autoplay policy" to "No user gesture is required".
									</Tooltip>
								)}>
									<FontAwesomeIcon icon={faExclamationTriangle}
										style={{color: "yellow", marginLeft: "10px"}} />
								</OverlayTrigger>
							</ControlLabel>
							<FormControl
								onChange={e => this.setState({ ytMus: e.target.value })}
								type="text"
								value={this.state.ytMus}
								placeholder="YouTube URL..."
							/>
						</FormGroup>
					</Modal.Body>
					<Modal.Footer>
						<Button onClick={this.handleSave}>Save</Button>
					</Modal.Footer>
				</Modal>
			</div>
		);
	}

	handleClose = event => {
		this.setState({ inputServer: this.state.server });
		if (getCookie("bgImg")) {
			this.setState({ bgImg: getCookie("bgImg") });
		} else {
			this.setState({ bgImg: "" });
		}
		if (getCookie("bgImgStyle")) {
			this.setState({ bgImgStyle: getCookie("bgImgStyle") });
		} else {
			this.setState({ bgImgStyle: "zoomed" });
		}
		if (getCookie("bgColorType")) {
			this.setState({ bgColorType: (getCookie("bgColorType") === 'true') });
		} else {
			this.setState({ bgColorType: true });
		}
		if (getCookie("bgColor")) {
			this.setState({ bgColor: getCookie("bgColor") });
		} else {
			this.setState({ bgColor: "#2b3e50" });
		}
		if (getCookie("ytMus")) {
			this.setState({ ytMus: getCookie("ytMus") });
		} else {
			this.setState({ ytMus: "" });
		}
		this.setState({ settingsShow: false });
	}

	handleSave = event => {
		this.setState({ error: '' });
		setCookie("server", this.state.inputServer, 365*5);
		this.setState({ server: this.state.inputServer });
		/* Set bg color before bg image */
		setCookie("bgColorType", this.state.bgColorType, 365*5);
		window.bgColorType = this.state.bgColorType;
		setCookie("bgColor", this.state.bgColor, 365*5);
		if(!this.state.bgColorType) {
			document.documentElement.style.setProperty('--bodyBg', this.state.bgColor);
		}
		setCookie("bgImg", this.state.bgImg, 365*5);
		setCookie("bgImgStyle", this.state.bgImgStyle, 365*5);
		setBgImg(this.state.bgImg, this.state.bgImgStyle );
		setVarColors(this, this.state.bgImg, this.state.bgImgStyle);
		setCookie("ytMus", this.state.ytMus, 365*5);
		PlayYTMusic(getIDFromYTURL(this.state.ytMus));
		this.setState({ settingsShow: false });
	}

	handleShow = event => {
		this.setState({ settingsShow: true });
	}

	loginEnable() {
		try {
			document.getElementById("loginBtn").disabled = false;
		} catch(e) {}
	}

	loginDisable() {
		try {
			document.getElementById("loginBtn").disabled = true;
		} catch(e) {}
	}

}
