import React from "react";
import { Route, Switch } from "react-router-dom";
import Home from "./containers/Home";
import Clients from "./containers/Clients";
import NotFound from "./containers/NotFound";

export default () =>
	<Switch>
		<Route path="/" exact component={Home} />
		<Route path="/clients" exact component={Clients} />
		{ /*<Route path="/logout" exact component={Logout} /> */}

		{ /* Finally, catch all unmatched routes */ }
		<Route component={NotFound} />
	</Switch>;
